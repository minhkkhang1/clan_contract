#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>

using namespace eosio;

namespace eosiotoken {
static constexpr name EOSIOTOKEN_ACCOUNT = name("eosio.token");

struct accounts {
  eosio::asset balance;
  uint64_t primary_key() const { return balance.symbol.code().raw(); }
};
typedef eosio::multi_index<eosio::name("accounts"), accounts> accounts_table;

accounts_table get_accounts(name account_name) {
  return accounts_table(EOSIOTOKEN_ACCOUNT, account_name.value);
}

} // namespace eosiotoken