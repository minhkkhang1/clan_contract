#include <eosio/asset.hpp>
#include <eosio/crypto.hpp>
#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>
#include <eosio/system.hpp>

#include <atomicassets-interface.hpp>
#include <eosio.token.interfaces.hpp>

using namespace eosio;
using namespace std;

static constexpr symbol CORE_TOKEN_SYMBOL = symbol("WAX", 8);

CONTRACT clan_contract : public contract {
public:
  using contract::contract;

  ACTION init();
  using init_action = action_wrapper<"init"_n, &clan_contract::init>;

  ACTION withdraw(name owner, asset token_to_withdraw);
  using withdraw_action = action_wrapper<"withdraw"_n, &clan_contract::withdraw>;

  ACTION joinclan(name clan, name actor);
  using joinclan_action = action_wrapper<"joinclan"_n, &clan_contract::joinclan>;

  ACTION kickmember(name clan, name member);
  using kickmember_action = action_wrapper<"kickmember"_n, &clan_contract::kickmember>;

  ACTION leaveclan(name clan, name actor);
  using leaveclan_action = action_wrapper<"leaveclan"_n, &clan_contract::leaveclan>;

  ACTION updatefee(name clan, asset fee);
  using updatefee_action = action_wrapper<"updatefee"_n, &clan_contract::updatefee>;

  ACTION uclanavatar(name clan, uint64_t id);
  using uclanavatar_action = action_wrapper<"uclanavatar"_n, &clan_contract::uclanavatar>;

  ACTION addavatar(string img);
  using addavatar_action = action_wrapper<"addavatar"_n, &clan_contract::addavatar>;

  ACTION updateavatar(uint64_t id, string img);
  using updateavatar_action = action_wrapper<"updateavatar"_n, &clan_contract::updateavatar>;

  ACTION removeavatar(uint64_t id);
  using removeavatar_action = action_wrapper<"removeavatar"_n, &clan_contract::removeavatar>;

  void receive_token_transfer(name from, name to, asset quantity, string memo);

  void receive_asset_transfer(name from, name to, vector<uint64_t> asset_ids,
                              string memo);

private:
  TABLE balances_table {
    name owner;
    vector<asset> quantities;

    uint64_t primary_key() const { return owner.value; };
  };
  typedef multi_index<name("balances"), balances_table> balances_index;

  TABLE members_table {
    name member_name;
    name clan_name;
    uint32_t join_date;

    uint64_t primary_key() const { return member_name.value; };
    uint64_t by_secondary() const { return clan_name.value; }
  };
  typedef eosio::multi_index<
    name("members"), members_table,
    eosio::indexed_by<
      name("clan"),
      eosio::const_mem_fun<members_table, uint64_t, &members_table::by_secondary>>> members_index;
  

  TABLE clans_table {
    name owner;
    name clan_name;
    uint16_t count = 0;
    uint32_t create_date;
    uint16_t max_count = 100;
    asset fee;
    uint64_t avatar_id;
    string avatar;

    uint64_t primary_key() const { return clan_name.value; };
    uint64_t by_secondary() const { return avatar_id; };
  };
  typedef eosio::multi_index<
    name("clans"), clans_table,
    eosio::indexed_by<
      name("avatar"),
      eosio::const_mem_fun<clans_table, uint64_t, &clans_table::by_secondary>>> clans_index;

  TABLE avatars_table {
    uint64_t id;
    string img;

    uint64_t primary_key() const { return id; };
  };
  typedef eosio::multi_index<name("avatars"), avatars_table> avatars_index;

  TABLE clanassets_table {
    uint64_t asset_id;
    name owner;
    name clan_name;
    uint32_t unstaking_time;

    uint64_t primary_key() const { return asset_id; };
    uint64_t by_secondary() const { return clan_name.value; };
  };
  typedef eosio::multi_index<
    name("clanassets"), clanassets_table,
    eosio::indexed_by<
      name("clan"),
      eosio::const_mem_fun<clanassets_table, uint64_t, &clanassets_table::by_secondary>>> clanassets_index;

  TABLE config_table {
    uint64_t avatar_counter = 0;
    asset min_fee = asset(000000000, CORE_TOKEN_SYMBOL);
    asset max_fee = asset(500000000, CORE_TOKEN_SYMBOL);
    uint16_t max_clan_member = 100;
  };
  typedef singleton<name("config"), config_table> config_index;
  typedef multi_index<name("config"), config_table> config_index_for_abi;

  balances_index balances = balances_index(get_self(), get_self().value);
  clans_index clans = clans_index(get_self(), get_self().value);
  members_index members = members_index(get_self(), get_self().value);
  avatars_index avatars = avatars_index(get_self(), get_self().value);
  clanassets_index clanassets = clanassets_index(get_self(), get_self().value);
  config_index config = config_index(get_self(), get_self().value);

  bool is_token_supported(symbol token_symbol);

  void internal_withdraw_tokens(name withdrawer, asset quantity, string memo);

  void internal_add_balance(name owner, asset quantity);

  void internal_decrease_balance(name owner, asset quantity);

  void create_member(name member_name, name clan_name);

  void create_clan(name clan_name, name owner);

  void stake_clan_asset(vector<uint64_t> asset_ids, name actor);
};

extern "C" void apply(uint64_t receiver, uint64_t code, uint64_t action) {
  if (code == receiver) {
    switch (action) {
      EOSIO_DISPATCH_HELPER(
          clan_contract,
          (init)(joinclan)(withdraw)(kickmember)(leaveclan)(updatefee)(uclanavatar)(addavatar)(updateavatar)(removeavatar))
    }
  } else if (code == atomicassets::ATOMICASSETS_ACCOUNT.value &&
             action == name("transfer").value) {
    eosio::execute_action(name(receiver), name(code),
                          &clan_contract::receive_asset_transfer);

  } else if (action == name("transfer").value) {
    eosio::execute_action(name(receiver), name(code),
                          &clan_contract::receive_token_transfer);
  }
}
