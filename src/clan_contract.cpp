#include <clan_contract.hpp>
#include <math.h>
ACTION clan_contract::init() {
  require_auth(get_self());
  config.get_or_create(get_self(), config_table{});
}

ACTION clan_contract::joinclan(name clan, name actor) {
  require_auth(actor);
  auto clan_itr =
      clans.require_find(clan.value, "No clan with this name exists");
  check(clan_itr->count < clan_itr->max_count, "This clan is full");

  auto member_itr = members.find(actor.value);
  check(member_itr == members.end(), "User already is a member of a clan");

  internal_decrease_balance(actor, clan_itr->fee);
  internal_add_balance(clan_itr->owner, clan_itr->fee);

  create_member(actor, clan);
}

/**
 * Withdraws a token from a users balance. The specified token is then
 * transferred to the user.
 *
 * @required_auth owner
 */
ACTION clan_contract::withdraw(name owner, asset token_to_withdraw) {
  require_auth(owner);

  check(token_to_withdraw.is_valid(), "Invalid type token_to_withdraw");

  internal_withdraw_tokens(owner, token_to_withdraw, "Token Withdrawal");
}
ACTION clan_contract::kickmember(name clan, name member) {
  auto clan_itr =
      clans.require_find(clan.value, "No clan with this name exists");
  require_auth(clan_itr->owner);

  auto member_itr =
      members.require_find(member.value, "This member is not a member of any clan");
  check(member_itr->clan_name == clan, "This member is not a member of the provided clan");
  members.erase(member_itr);

  auto count = clan_itr->count - 1;
  clans.modify(clan_itr, get_self(), [&](auto &_clan) { _clan.count = count; });
}

ACTION clan_contract::leaveclan(name clan, name actor) {
  auto clan_itr =
      clans.require_find(clan.value, "No clan with this name exists");
  require_auth(actor);

  auto member_itr =
      members.require_find(actor.value, "User is not a member of any clan");
  check(member_itr->clan_name == clan, "User is not a member of the provided clan");
  members.erase(member_itr);

  auto count = clan_itr->count - 1;
  clans.modify(clan_itr, get_self(), [&](auto &_clan) { _clan.count = count; });
}

ACTION clan_contract::updatefee(name clan, asset fee) {
  check(fee.is_valid(), "Invalid type fee");
  check(is_token_supported(fee.symbol),
        "The transferred token is not supported");

  auto clan_itr =
      clans.require_find(clan.value, "No clan with this name exists");
  require_auth(clan_itr->owner);

  config_table current_config = config.get();
  check(fee.amount > current_config.min_fee.amount, "Fee must be greater than zero");
  check(fee.amount <= current_config.max_fee.amount, "Max fee is 5 WAX");

  clans.modify(clan_itr, get_self(), [&](auto &_clan) { _clan.fee = fee; });
}

ACTION clan_contract::uclanavatar(name clan, uint64_t id) {
  auto clan_itr =
      clans.require_find(clan.value, "No clan with this name exists");
  require_auth(clan_itr->owner);
  auto avatar_itr = avatars.require_find(id, "No avatar with this id exists");
  check(clan_itr->avatar_id != avatar_itr->id, "Clan already has this avatar");
  clans.modify(clan_itr, get_self(), [&](auto &_clan) {
    _clan.avatar_id = avatar_itr->id;
    _clan.avatar = avatar_itr->img;
  });
}
ACTION clan_contract::addavatar(string img) {
  require_auth(get_self());
  config_table current_config = config.get();
  uint64_t avatar_id = current_config.avatar_counter + 1;
  current_config.avatar_counter++;
  config.set(current_config, get_self());

  avatars.emplace(get_self(), [&](auto &_avatar) {
    _avatar.id = avatar_id;
    _avatar.img = img;
  });
}
ACTION clan_contract::updateavatar(uint64_t id, string img) {
  require_auth(get_self());
  auto avatar_itr = avatars.require_find(id, "No avatar with this id exists");

  auto avatar_index = clans.get_index<name("avatar")>();
  auto clan_itr = avatar_index.lower_bound(id);
  auto end_itr = avatar_index.upper_bound(id);
  for (; clan_itr != end_itr; clan_itr++) {
    if (clan_itr->avatar_id == id) {
      avatar_index.modify(clan_itr, get_self(), [&](auto &_clan) { 
        _clan.avatar = img;
      });
    }
  }

  avatars.modify(avatar_itr, get_self(), [&](auto &_avatar) {
    _avatar.img = img;
  });
}
ACTION clan_contract::removeavatar(uint64_t id) {
  require_auth(get_self());
  auto avatar_itr = avatars.require_find(id, "No avatar with this id exists");

  avatars.erase(avatar_itr); 

  auto avatar_index = clans.get_index<name("avatar")>();
  auto clan_itr = avatar_index.lower_bound(id);
  auto end_itr = avatar_index.upper_bound(id);

  for (; clan_itr != end_itr; clan_itr++) {
    if (clan_itr->avatar_id == id) {
      avatar_index.modify(clan_itr, get_self(), [&](auto &_clan) { 
        _clan.avatar_id = 0; 
        _clan.avatar = "";
      });
    }
  }
}

uint32_t now() {
    return (uint32_t)(eosio::current_time_point().sec_since_epoch());
}

/**
 * Condition is validated from action so this function only create member for
 * the clan without checking anything
 */
void clan_contract::create_member(name member_name, name clan_name) {
  auto clan_itr =
      clans.require_find(clan_name.value, "No clan with this name exists");

  auto count = clan_itr->count + 1;
  clans.modify(clan_itr, get_self(), [&](auto &_clan) { _clan.count = count; });
  uint32_t join_date = now();
  members.emplace(member_name, [&](auto &_member) {
    _member.member_name = member_name;
    _member.clan_name = clan_name;
    _member.join_date = join_date;
  });
}

void clan_contract::create_clan(name clan_name, name owner) {
  check(clans.find(clan_name.value) == clans.end(),
        "A clan with this name already exists");

  uint32_t create_date = now();
  config_table current_config = config.get();
  clans.emplace(_self, [&](auto &row) {
      row.clan_name = clan_name;
      row.owner = owner;
      row.count = 1;
      row.max_count = current_config.max_clan_member;
      row.create_date = create_date;
      row.fee = current_config.max_fee;
      row.avatar_id = 0;
      row.avatar = "";
  });
  members.emplace(_self, [&](auto &row) {
      row.member_name = owner;
      row.clan_name = clan_name;
      row.join_date = create_date;
  });
}

void clan_contract::stake_clan_asset(vector<uint64_t> asset_ids, name actor) {
  auto member_itr = members.require_find(actor.value, "Please join a clan first to stake clan asset");

  auto asset_clan_index = clanassets.get_index<name("clan")>();
  auto clanasset_itr = asset_clan_index.lower_bound((member_itr->clan_name).value);
  auto asset_end_itr = asset_clan_index.upper_bound((member_itr->clan_name).value);
  for (; clanasset_itr != asset_end_itr; clanasset_itr++) {
    for(auto a_id : asset_ids) {
      check(clanasset_itr->asset_id == a_id, "Asset is already stacked in this clan");
    }
  }

  uint32_t stake_date = now();
  for(auto a_id : asset_ids) {
    clanassets.emplace(_self, [&](auto &row) {
      row.asset_id = a_id;
      row.owner = actor;
      row.clan_name = member_itr->clan_name;
      row.unstaking_time = stake_date + 259200;
    });
  }
}

/**
 * Decreases the withdrawers balance by the specified quantity and transfers the
 * tokens to them Throws if the withdrawer does not have a sufficient balance
 */
void clan_contract::internal_withdraw_tokens(name withdrawer, asset quantity,
                                          string memo) {
  check(quantity.amount > 0, "The quantity to withdraw must be positive");

  // This will throw if the user does not have sufficient balance
  internal_decrease_balance(withdrawer, quantity);

  action(permission_level{get_self(), name("active")}, name("eosio.token"),
         name("transfer"), make_tuple(get_self(), withdrawer, quantity, memo))
      .send();
}

/**
 * Internal function used to add a quantity of a token to an account's balance
 * It is not checked whether the added token is a supported token, this has to
 * be checked before calling this function
 */
void clan_contract::internal_add_balance(name owner, asset quantity) {
  if (quantity.amount == 0) {
    return;
  }
  check(quantity.amount > 0, "Can't add negative balances");

  auto balance_itr = balances.find(owner.value);

  vector<asset> quantities;
  if (balance_itr == balances.end()) {
    // No balance table row exists yet
    quantities = {quantity};
    balances.emplace(get_self(), [&](auto &_balance) {
      _balance.owner = owner;
      _balance.quantities = quantities;
    });

  } else {
    // A balance table row already exists for owner
    quantities = balance_itr->quantities;

    bool found_token = false;
    for (asset &token : quantities) {
      if (token.symbol == quantity.symbol) {
        // If the owner already has a balance for the token, this balance is
        // increased
        found_token = true;
        token.amount += quantity.amount;
        break;
      }
    }

    if (!found_token) {
      // If the owner does not already have a balance for the token, it is added
      // to the vector
      quantities.push_back(quantity);
    }

    balances.modify(balance_itr, get_self(),
                    [&](auto &_balance) { _balance.quantities = quantities; });
  }
}

/**
 * Internal function used to deduct a quantity of a token from an account's
 * balance If the account does not has less than that quantity in his balance,
 * this function will cause the transaction to fail
 */
void clan_contract::internal_decrease_balance(name owner, asset quantity) {
  auto balance_itr = balances.require_find(
      owner.value, "The specified account does not have a balance table row");

  vector<asset> quantities = balance_itr->quantities;
  bool found_token = false;
  for (auto itr = quantities.begin(); itr != quantities.end(); itr++) {
    if (itr->symbol == quantity.symbol) {
      found_token = true;
      check(itr->amount >= quantity.amount,
            "The specified account's balance is lower than the specified "
            "quantity");
      itr->amount -= quantity.amount;
      if (itr->amount == 0) {
        quantities.erase(itr);
      }
      break;
    }
  }
  check(found_token, "The specified account does not have a balance for the "
                     "symbol specified in the quantity");

  // Updating the balances table
  if (quantities.size() > 0) {
    balances.modify(balance_itr, same_payer,
                    [&](auto &_balance) { _balance.quantities = quantities; });
  } else {
    balances.erase(balance_itr);
  }
}

/**
 * Internal function to check whether an token is a supported token
 */
bool clan_contract::is_token_supported(symbol token_symbol) {
  if (token_symbol == CORE_TOKEN_SYMBOL) {
    return true;
  }
  return false;
}

/**
 * This function is called when a transfer receipt from any token contract is
 * sent to this contract It handles deposits and adds the transferred tokens to
 * the sender's balance table row
 */
void clan_contract::receive_token_transfer(name from, name to, asset quantity,
                                        string memo) {
  if (to != get_self()) {
    return;
  }

  check(is_token_supported(quantity.symbol),
        "The transferred token is not supported");

  if (memo == "deposit") {
    internal_add_balance(from, quantity);
  } else {
    check(false, "invalid memo");
  }
}

/**
 * This function is called when a "transfer" action receipt from the
 * atomicassets contract is sent to the atomicmarket contract. It handles
 * receiving assets for auctions.
 */
void clan_contract::receive_asset_transfer(name from, name to,
                                        vector<uint64_t> asset_ids,
                                        string memo) {
  if (to != get_self()) {
    return;
  }

  require_auth(from);

  string create_clan_prefix = "createclan:";
  string stake_clan_asset_prefix = "clan_stake_asset";

  if (memo.rfind(create_clan_prefix, 0) == 0) {
    auto asset_itr =
        atomicassets::get_assets(get_self())
            .require_find(asset_ids.at(0), "No asset with this id exists");
    check(asset_itr->template_id == 161044, "Invalid clan card");

    string n = memo.substr(create_clan_prefix.length());
    auto clan_name = name(n);

    clan_contract::create_clan(clan_name, from);
  } else {
    if (memo.rfind(stake_clan_asset_prefix, 0) == 0) {
      clan_contract::stake_clan_asset(asset_ids, from);
    }
    else {
      check(false, "Invalid memo");
    }
  }
}